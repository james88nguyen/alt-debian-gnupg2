Unofficial GnuPG 2.1 packaged for Jessie
========================================

No longer updated
-----------------

**Since jessie became oldstable, this will no longer be maintained.** I have no personal interest in the package anymore. The version here is what I used myself on jessie. It was so far unreleased because I hadn't tested the WKS packages yet. The WKS and gpgsm packages are untested, the rest worked fine for me.

What is it
----------

This is a backport of the latest Debian GnuPG 2.1 packages to Jessie. Whereas the versions in stretch and sid provide the binary /usr/bin/gpg and the old 1.4 branch has been dropped, this version still installs as /usr/bin/gpg2 and allows co-installation with GnuPG 1.4.

Usage warning
-------------

**Use at your own risk! This is provided as-is without any warranties. Once you start manually managing these packages, you will also no longer receive security updates from jessie-security and stuff like that.**

**IMPORTANT:** Simultaneously using both GnuPG 1.4 and GnuPG 2.1 is not recommended. Once you start using 2.1, keyrings can go out of sync between 1.4 and 2.1 and you will likely get confused. Check the programs that you use which use GnuPG, and be very aware of which version they are using.

I'm giving you all the loaded gun to shoot yourself in the foot. I do not recommend my fork for general use, but rather as a service to people who really want 2.1 and are prepared to deal with issues arising from having both 1.4 and 2.1 on the same system.

This version will replace jessie's 2.0.26 with 2.1.19, but it will install next to GnuPG 1.4. However, mixing 1.4 and 2.1 is not for the faint of heart. There are good reasons that the official Debian package is choosing not to support 1.4 and 2.1 on the same machine starting with Debian stretch: while on the surface they are compatible, they quickly go out of sync regarding private keys and it can get interesting with public keys as well, and not in a good way.


**I did not test gpgsm.** The rest received some testing and seemed to work okay for me.

I reserve the right to be lazy. I could be behind on packaging.

What you need as well
---------------------

Install these packages from jessie-backports:

* debhelper and dependencies
* libassuan-dev, libassuan, libgcrypt20-dev, libgcrypt20, libgpg-error-dev, libgpg-error0, libksba-dev, libksba8, libnpth0-dev and libntph0

Only debhelper pulled in extra dependencies from jessie-backports for me, the others don't need any additional dependencies.

It might work with some of these packages from jessie as well, but the newer versions contain enhancements and fixes that are useful to have. That's why I recommend getting all these packages as per above.

Note that in an earlier version, this README advised to get stuff from stretch or sid. Since then, jessie-backports picked up the relevant packages.

What did I change
-----------------

* I dropped the win32 packages, gpgv-static and gpgv-udeb. I gave them short consideration, but once they presented their own issues, I immediately dropped them. gpgv-udeb is not used by the Debian Installer for jessie, and I reason that you can simply get the other packages from stretch or sid since they aren't meant to run on Debian itself anyway.

* Systemd user session management of the daemons was disabled by default again, since it caused gpg-agent to lock up for me if it were enabled.

Final thoughts
--------------

I hope I did everything right, I don't work with other people's source that often. You are encouraged to review my changes and speak up if something seems incorrect or missing. I'd also like to hear it if you are using this, but don't feel obliged to tell me.

I based my work largely on the difference between Debian release 2.1.11-7 and 2.1.11-7+exp1, which is where GnuPG 2.1 starts providing stuff previously provided by GnuPG 1.4.
